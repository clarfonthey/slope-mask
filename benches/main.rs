use criterion::{Criterion, criterion_group, criterion_main};

fn unit(_: &mut Criterion) {}

criterion_group!(benches, unit);
criterion_main!(benches);
