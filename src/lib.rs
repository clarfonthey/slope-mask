//! Very niche data structure designed for blockpick.
#![cfg_attr(not(test), no_std)]
#![cfg_attr(feature = "nightly", feature(const_trait_impl))]
#![cfg_attr(feature = "nightly", feature(coverage_attribute))]
#![cfg_attr(feature = "nightly", feature(derive_const))]
