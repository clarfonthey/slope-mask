# slope-mask

Very niche data structure designed for blockpick.

## License

Available via the [Anti-Capitalist Software License][ACSL] for individuals, non-profit
organisations, and worker-owned businesses.

[ACSL]: ./LICENSE.md
